# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Komikku package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Komikku 0.12.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-26 02:07+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../komikku/main_window.py:226
msgid "Contributors: Code, Patches, Debugging:"
msgstr ""

#: ../komikku/main_window.py:253
msgid "Are you sure you want to quit?"
msgstr ""

#: ../komikku/main_window.py:256
msgid "Some chapters are currently being downloaded."
msgstr ""

#: ../komikku/main_window.py:258
msgid "Some mangas are currently being updated."
msgstr ""

#: ../komikku/main_window.py:261
msgid "Quit?"
msgstr ""

#: ../komikku/reader/pager/__init__.py:419
msgid "There is no previous chapter."
msgstr ""

#: ../komikku/reader/pager/__init__.py:421
msgid "It was the last chapter."
msgstr ""

#: ../komikku/reader/pager/page.py:270
msgid "Retry"
msgstr ""

#: ../komikku/updater.py:71
msgid "Library update completed"
msgstr ""

#: ../komikku/updater.py:73
msgid "Update completed"
msgstr ""

#: ../komikku/updater.py:83
msgid "No new chapter found"
msgstr ""

#: ../komikku/updater.py:106
#, python-brace-format
msgid ""
"{0}\n"
"Oops, update has failed. Please try again."
msgstr ""

#: ../komikku/updater.py:114
msgid "Library update started"
msgstr ""

#: ../komikku/updater.py:116
msgid "Update started"
msgstr ""

#: ../komikku/models/database.py:234
msgid "Complete"
msgstr ""

#: ../komikku/models/database.py:235
msgid "Ongoing"
msgstr ""

#: ../komikku/models/database.py:236
msgid "Suspended"
msgstr ""

#: ../komikku/models/database.py:237
msgid "Hiatus"
msgstr ""

#: ../komikku/models/database.py:678
msgid "Download pending"
msgstr ""

#: ../komikku/models/database.py:679
msgid "Downloaded"
msgstr ""

#: ../komikku/models/database.py:680
msgid "Downloading"
msgstr ""

#: ../komikku/models/database.py:681
msgid "Download error"
msgstr ""

#: ../komikku/card.py:85
#, python-brace-format
msgid ""
"NOTICE\n"
"{0} server is not longer supported.\n"
"Please switch to another server."
msgstr ""

#: ../komikku/card.py:136 ../komikku/library.py:173
msgid "Delete?"
msgstr ""

#: ../komikku/card.py:137
msgid "Are you sure you want to delete this manga?"
msgstr ""

#: ../komikku/card.py:367
msgid "New"
msgstr ""

#: ../komikku/card.py:383
msgid "%m/%d/%Y"
msgstr ""

#: ../komikku/card.py:461
msgid "Reset"
msgstr ""

#: ../komikku/card.py:463
msgid "Download"
msgstr ""

#: ../komikku/card.py:465
msgid "Mark as read"
msgstr ""

#: ../komikku/card.py:467
msgid "Mark as unread"
msgstr ""

#: ../komikku/card.py:581
#, python-brace-format
msgid "Disk space used: {0}"
msgstr ""

#: ../komikku/add_dialog.py:125
#, python-brace-format
msgid "{0} manga added"
msgstr ""

#: ../komikku/add_dialog.py:228
msgid "MOST POPULARS"
msgstr ""

#: ../komikku/add_dialog.py:261
msgid "Oops, search failed. Please try again."
msgstr ""

#: ../komikku/add_dialog.py:263
msgid "No results"
msgstr ""

#: ../komikku/add_dialog.py:339
msgid "Oops, failed to retrieve manga's information."
msgstr ""

#: ../komikku/add_dialog.py:363
#, python-brace-format
msgid "Search in {0}…"
msgstr ""

#: ../komikku/utils.py:22
msgid "No Internet connection or server down"
msgstr ""

#: ../komikku/library.py:174
msgid "Are you sure you want to delete selected mangas?"
msgstr ""

#: ../komikku/application.py:46 ../komikku/application.py:50
msgid "Komikku"
msgstr ""

#: ../komikku/settings_dialog.py:92
msgid "Right to Left ←"
msgstr ""

#: ../komikku/settings_dialog.py:93
msgid "Left to Right →"
msgstr ""

#: ../komikku/settings_dialog.py:94
msgid "Vertical ↓"
msgstr ""

#: ../komikku/settings_dialog.py:103
msgid "Adapt to Screen"
msgstr ""

#: ../komikku/settings_dialog.py:104
msgid "Adapt to Width"
msgstr ""

#: ../komikku/settings_dialog.py:105
msgid "Adapt to Height"
msgstr ""

#: ../komikku/settings_dialog.py:114
msgid "White"
msgstr ""

#: ../komikku/settings_dialog.py:115
msgid "Black"
msgstr ""

#: ../komikku/downloader.py:158
msgid "Download completed"
msgstr ""

#: ../komikku/downloader.py:159 ../komikku/downloader.py:188
#, python-brace-format
msgid "[{0}] Chapter {1}"
msgstr ""

#: ../komikku/downloader.py:182
#, python-brace-format
msgid "{0}/{1} pages downloaded"
msgstr ""

#: ../komikku/downloader.py:184
msgid "error"
msgstr ""

#: ../data/ui/about_dialog.ui.in:11
msgid ""
"An online/offline manga reader.\n"
"\n"
"Never forget, you can support the authors\n"
"by buying the official comics when they are\n"
"available in your region/language."
msgstr ""

#: ../data/ui/about_dialog.ui.in:17
msgid "Learn more about Komikku"
msgstr ""

#: ../data/ui/add_dialog.ui:47
msgid "Select a server"
msgstr ""

#: ../data/ui/add_dialog.ui:270 ../data/ui/main_window.ui:180
msgid "Authors"
msgstr ""

#: ../data/ui/add_dialog.ui:300 ../data/ui/main_window.ui:210
msgid "Genres"
msgstr ""

#: ../data/ui/add_dialog.ui:330 ../data/ui/main_window.ui:240
msgid "Status"
msgstr ""

#: ../data/ui/add_dialog.ui:360 ../data/ui/main_window.ui:270
msgid "Server"
msgstr ""

#: ../data/ui/add_dialog.ui:391 ../data/ui/main_window.ui:301
msgid "Synopsis"
msgstr ""

#: ../data/ui/add_dialog.ui:423 ../data/ui/main_window.ui:389
msgid "Scanlators"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:14
msgctxt "Shortcut window description"
msgid "Application"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:18
msgctxt "Shortcut window description"
msgid "Add manga"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:25
msgctxt "Shortcut window description"
msgid "Open preferences"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:32
msgctxt "Shortcut window description"
msgid "Toggle Fullscreen mode"
msgstr ""

#. Title of the settings dialog
#: ../data/ui/settings_dialog.ui:16
msgid "Settings"
msgstr ""

#: ../data/ui/settings_dialog.ui:64
msgid "General"
msgstr ""

#: ../data/ui/settings_dialog.ui:115
msgid "Dark Theme"
msgstr ""

#: ../data/ui/settings_dialog.ui:131
msgid "Use dark GTK theme"
msgstr ""

#: ../data/ui/settings_dialog.ui:198
msgid "Night Light"
msgstr ""

#: ../data/ui/settings_dialog.ui:214
msgid "Automatically enable dark theme at night"
msgstr ""

#: ../data/ui/settings_dialog.ui:281
msgid "Desktop Notifications"
msgstr ""

#: ../data/ui/settings_dialog.ui:297
msgid "Use desktop notifications for downloads"
msgstr ""

#: ../data/ui/settings_dialog.ui:355 ../data/ui/main_window.ui:588
msgid "Library"
msgstr ""

#: ../data/ui/settings_dialog.ui:404
msgid "Update at Startup"
msgstr ""

#: ../data/ui/settings_dialog.ui:420
msgid "Automatically update library at startup"
msgstr ""

#: ../data/ui/settings_dialog.ui:466
msgid "Restrict servers to selected languages"
msgstr ""

#: ../data/ui/settings_dialog.ui:467
msgid "Servers Languages"
msgstr ""

#: ../data/ui/settings_dialog.ui:489
msgid "Reader"
msgstr ""

#: ../data/ui/settings_dialog.ui:517
msgid "Reading Direction"
msgstr ""

#: ../data/ui/settings_dialog.ui:524
msgid "Type of scaling to adapt image"
msgstr ""

#: ../data/ui/settings_dialog.ui:525
msgid "Scaling"
msgstr ""

#: ../data/ui/settings_dialog.ui:533 ../data/ui/menu/reader.xml:53
msgid "Background Color"
msgstr ""

#: ../data/ui/settings_dialog.ui:562 ../data/ui/menu/reader.xml:71
msgid "Crop borders"
msgstr ""

#: ../data/ui/settings_dialog.ui:579
msgid "Crop white borders of images"
msgstr ""

#: ../data/ui/settings_dialog.ui:645
msgid "Fullscreen"
msgstr ""

#: ../data/ui/settings_dialog.ui:662
msgid "Automatically enter fullscreen mode"
msgstr ""

#: ../data/ui/menu/download_manager_selection_mode.xml:7
#: ../data/ui/menu/library_selection_mode.xml:11 ../data/ui/menu/card.xml:7
msgid "Delete"
msgstr ""

#: ../data/ui/menu/library_selection_mode.xml:7 ../data/ui/menu/card.xml:11
msgid "Update"
msgstr ""

#: ../data/ui/menu/library_selection_mode.xml:18
#: ../data/ui/menu/card_selection_mode.xml:26
msgid "Select All"
msgstr ""

#: ../data/ui/menu/card.xml:18
msgid "Order of Chapters"
msgstr ""

#: ../data/ui/menu/card.xml:23
msgid "By Chapter Number (9-0)"
msgstr ""

#: ../data/ui/menu/card.xml:28
msgid "By Chapter Number (0-9)"
msgstr ""

#: ../data/ui/menu/card.xml:38
msgid "Open in Browser"
msgstr ""

#: ../data/ui/menu/card_selection_mode.xml:15
msgid "Mark as Read"
msgstr ""

#: ../data/ui/menu/card_selection_mode.xml:19
msgid "Mark as Unread"
msgstr ""

#: ../data/ui/menu/reader.xml:7
msgid "Reading direction"
msgstr ""

#: ../data/ui/menu/reader.xml:30
msgid "Type of Scaling"
msgstr ""

#: ../data/ui/menu/download_manager.xml:7
msgid "Delete All"
msgstr ""

#: ../data/ui/menu/main.xml:7
msgid "Update Library"
msgstr ""

#: ../data/ui/menu/main.xml:11 ../data/ui/download_manager_dialog.ui:40
msgid "Download Manager"
msgstr ""

#: ../data/ui/menu/main.xml:17
msgid "Preferences"
msgstr ""

#: ../data/ui/menu/main.xml:21
msgid "Keyboard Shortcuts"
msgstr ""

#: ../data/ui/menu/main.xml:25
msgid "About Komikku"
msgstr ""

#. The subtitle of the umbrella sentence in the first start screen. This is a sentence which gives the user a starting point what he can do if he opens the application for the first time.
#: ../data/ui/main_window.ui:61
msgid "An online/offline manga reader"
msgstr ""

#: ../data/ui/main_window.ui:333
msgid "Last update"
msgstr ""

#: ../data/ui/main_window.ui:409
msgid "Info"
msgstr ""

#: ../data/ui/main_window.ui:440
msgid "Chapters"
msgstr ""

#: ../data/ui/download_manager_dialog.ui:170
msgid "No downloads"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:3
msgid "@prettyname@"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:7
msgid "@appid@"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:13
msgid "manga;reader;viewer;comic;webtoon;scan;offline;"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:9
msgid "An online/offline manga reader for GNOME"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:11
msgid ""
"An online/offline manga reader for GNOME developed with the aim of being "
"used with the Librem 5 phone"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:26
msgid "Valéry Febvre"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:36
msgid "[Manga] Fixed the unread action of chapters"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:37
msgid "[Reader] Cursor hiding during keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:38
msgid "[Reader] Arrow scrolling in every reading directions"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:39
msgid "[Reader] Added white borders crop"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:40
msgid "[Servers] Added Kirei Cake (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:41
msgid "[Servers] Added Read Manga, Mint Manga and Self Manga (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:42
msgid "[Servers] Updated Mangakawaii domain"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:43
msgid "[Servers] Fixed JapScan search (using DuckDuckGo Lite)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:44
msgid "Added back navigation with Escape key"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:45
msgid ""
"Added abilility to add manga by ID using keyword \"id:&lt;id&gt;\" (useful "
"with MangaDex)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:46
msgid "Various bugs fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:51
msgid "Added the Dutch translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:56
msgid "Added Desu server (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:57
msgid "Added the Russian translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:58
msgid "Updated the Brazilian Portuguese translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:59
msgid "Fixed a bug in manga updater"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:64
msgid "Fixe a bug in Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:69
msgid "Added a Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:70
msgid "[Manga] Improved chapters download"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:71
msgid ""
"[Reader] Vertical reading direction: Added scrolling with UP and DOWN keys"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:72
msgid "[Reader] Added a Retry button when a image load failed"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:73
msgid "[Servers] Removed Manga Rock (server has closed)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:74
msgid "[Servers] Minor fix in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:75
msgid "[Servers] MangaDex: Added 15 languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:80
msgid "[Library] Add an unread badge"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:81
msgid "[Library/Manga] Added \"Select All\" in selection mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:82
msgid ""
"[Library/Manga] Now leaves selection mode when no mangas/chapters are "
"selected"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:83
msgid "[Manga] Improved chapters list rendering"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:84
msgid ""
"[Manga] Improved chapters download: add a progress bar and a \"Stop\" button"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:85
msgid "[Settings] General: New option to disable desktop notifications"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:86
msgid "[Servers] Minor fixes in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:87
msgid "[Servers] Add MangaDex (EN, ES, FR)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:92
msgid "Jaimini's Box server: Fixed mangas with an adult alert"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:97
msgid "Added MANGA Plus server (EN, ES)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:98
msgid "Added Jaimini's Box"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:103
msgid "Fixed 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:104
msgid "Fixed JapScan server search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:109
msgid ""
"Pager focus is now correctly restored when menu is closed (useful for keypad "
"navigation)."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:110
msgid "Happy new year to everyone."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:115
msgid "Added 'Vertical' reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:116
msgid "Added 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:121
msgid "Fixed Manganelo and WEBTOON servers"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:126
msgid "Ninemanga: Fixed missing chapters issue (EN, BR, DE, ES, IT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:127
msgid "Pepper&amp;Carrot: Fixed chapters update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:128
msgid "Pepper&amp;Carrot: Added missing Cover and Credits pages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:133
msgid "New preference: Servers languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:134
msgid "Bug fixes in Scantrad France and DB Multiverse"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:139
msgid "A bug fix in change of reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:140
msgid "A bug fix in Pepper&amp;Carrot"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:145
msgid "Added Pepper&amp;Carrot server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:146
msgid ""
"A free(libre) and open-source webcomic supported directly by its patrons to "
"change the comic book industry!"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:151
msgid "New preference: Automatically update library at startup"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:152
msgid "Fix in Mangakawaii server (Cloudflare problem)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:157
msgid "Fixes in Mangakawaii server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:162
msgid "Added Dragon Ball Multiverse (DBM) server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:163
msgid "Fixes in Japscan server (search still broken)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:168
msgid "New servers: NineManga Russian, Webtoon Indonesia and Webtoon Thai"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:169
msgid ""
"For all servers that allow it, most popular mangas are now offered as "
"starting search results."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:174
msgid "Improve speed of dates parsing"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:175
msgid "Fixed order of manga chapters for Scantrad France and Central de Mangás"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:180
msgid "Fix keyboard navigation in reader"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:185
msgid "Add a new server: xkcd (English)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:190
msgid "Add the Portuguese (Brazil) translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:191
msgid "Add a new server: Central de Mangas (Portuguese)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:196
msgid "Bug fix: Change the location of the data storage"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:201
msgid "First release"
msgstr ""
