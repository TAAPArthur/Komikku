import cloudscraper
from datetime import datetime
import json

from bs4 import BeautifulSoup
from komikku.servers import Server
from komikku.servers import USER_AGENT
import re
import os

SERVER_NAME = 'Crunchyroll'

headers = {
    'User-Agent': USER_AGENT,
    'Origin': 'https://www.crunchyroll.com/',
}


class Crunchyroll(Server):
    id = 'crunchyroll'
    name = SERVER_NAME
    lang = 'en'
    locale = 'enUS'

    BASE_URL = 'https://www.crunchyroll.com'
    LOGIN_URL = BASE_URL + '/login'
    MANGA_URL = BASE_URL + '/comics/manga/{0}'
    MANGA_INFO_URL = BASE_URL + '{0}'
    API_BASE_URL = 'http://api-manga.crunchyroll.com/'

    API_CHAPTER_LIST = API_BASE_URL + "list_chapter?session_id={}&chapter_id={}&auth={}"
    API_SERIES_URL = API_BASE_URL + 'series?sort=popular'
    API_CHAPTERS_URL = API_BASE_URL + 'chapters?series_id={}'

    CR_AUTH_URL = API_BASE_URL + "cr_authenticate?auth=&session_id={}&version=0&format=json"
    session_id = None
    cr_auth = None
    POSSIBLE_PAGE_URL_KEYS = ['encrypted_mobile_image_url', 'encrypted_composed_image_url']
    PAGE_URL_KEY = POSSIBLE_PAGE_URL_KEYS[0]

    USERNAME = os.environ.get('CR_USERNAME', None)
    PASSWORD = os.environ.get('CR_PASSWORD', None)

    def __init__(self):
        if self.session is None:
            self.session = cloudscraper.create_scraper()
            self.session.headers.update(headers)

    def get_manga_data(self, initial_data):
        """
        Returns manga data
        Initial data should contain at least manga's slug (provided by search)
        """
        assert 'slug' in initial_data, 'Manga slug is missing in initial data'
        r = self.session_get(self.API_CHAPTERS_URL.format(initial_data['slug']))

        json_data = r.json()
        resp_data = json_data["series"]
        chapters = json_data["chapters"]

        data = initial_data.copy()
        data.update(dict(
            authors=[resp_data.get("author", "")],
            scanlators=[resp_data.get("translator", "")],
            genres=resp_data.get("genres", ""),
            status=None,
            chapters=[],
            synopsis=resp_data["locale"][self.locale]["description"],
            server_id=self.id,
            cover=resp_data["locale"][self.locale]["thumb_url"],
        ))

        data['status'] = 'ongoing'

        # Chapters
        for chapter in chapters:
            data['chapters'].append(dict(
                slug=chapter['chapter_id'],
                title=chapter['number'],
                # date=chapter['availability_start'],
            ))

        return data

    def get_manga_chapter_url(self, chapter_id):
        self.login()
        return self.API_CHAPTER_LIST.format(Crunchyroll.session_id, chapter_id, self.cr_auth)

    def get_manga_chapter_data(self, manga_slug, chapter_slug, chapter_url):
        """
        Returns manga chapter data

        Currently, only pages are expected.
        """
        r = self.session_get(self.get_manga_chapter_url(chapter_slug))
        resp_data = r.json()
        pages = resp_data["pages"]
        pages.sort(key=lambda x: int(x["number"]))

        data = dict(
            pages=[],
            scrambled=0,
        )

        for page in pages:
            data['pages'].append(dict(
                slug=page["page_id"],  # not necessary, we know image URL already
                image=page["locale"][self.locale][self.PAGE_URL_KEY],
            ))

        return data

    def decodeImage(self, imageBuffer):
        # Don't know why 66 is special
        return bytes(b ^ k for (b, k) in zip(imageBuffer, [66] * len(imageBuffer)))

    def get_manga_chapter_page_image(self, manga_slug, manga_name, chapter_slug, page):
        """
        Returns chapter page scan (image) content
        """
        r = self.session_get(page['image'])

        return (page["slug"], self.decodeImage(r.content))

    def get_manga_url(self, slug, url):
        """
        Returns manga absolute URL
        """
        return self.MANGA_URL.format(slug)

    def get_most_populars(self):
        """
        Returns full list of manga sorted by rank
        """
        r = self.session_get(self.API_SERIES_URL)

        resp_data = r.json()
        result = []
        resp_data.sort(key=lambda x: not x["featured"])
        for item in resp_data:
            if "locale" in item:
                result.append({
                    "slug": item['series_id'],
                    "name": item["locale"][self.locale]["name"],
                    "synopsis": item["locale"][self.locale]["description"]
                })
        return result

    def search(self, term):
        term_lower = term.lower()
        return filter(lambda x: term_lower in x["name"].lower(), self.get_most_populars())

    def login(self, username=USERNAME, password=PASSWORD, force=False):
        """
        Setup Crunchyroll session and get the auth token
        """
        if not self.USERNAME or not self.PASSWORD:
            print("Need username and password to login")
            return
        if not self.setup_session(username, password, force):
            return

        if not self.cr_auth or force:
            data = self.session_get(self.CR_AUTH_URL.format(Crunchyroll.session_id)).json()
            self.cr_auth = "".join(data["data"]["auth"])

    def setup_session(self, username, password, force=False):
        """
        Initialize session_id
        Note that the login page is rate limited so call once and cache
        """
        if Crunchyroll.session_id and not force:
            return True
        page = BeautifulSoup(self.session_get(self.LOGIN_URL).text)
        hidden = page.findAll("input", {u"type": u"hidden"})[1].get("value")
        logindata = {'formname': 'login_form', 'fail_url': self.LOGIN_URL, 'login_form[name]': username, 'login_form[password]': password, 'login_form[_token]': hidden, 'login_form[redirect_url]': '/'}
        req = self.session_post(self.LOGIN_URL, data=logindata)
        html = self.session_get(self.BASE_URL).text
        if re.search(username + '(?i)', html):
            print('You have been successfully logged in.\n\n')

            match = re.search('sessionId: "(\w*)"', self.session_get('https://www.crunchyroll.com/manga/the-seven-deadly-sins/read/1').text)
            if match:
                Crunchyroll.session_id = match.group(1)
                print(Crunchyroll.session_id)
                return True
            else:
                print("Could not get sessionId")
        else:
            print('Failed to verify your username and/or password. Please try again.\n\n')
        return False
