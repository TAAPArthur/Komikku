
BUILD := _build

all:
	mkdir -p $(BUILD)
	meson . $(BUILD)
	meson configure $(BUILD) -Dprefix=$$(pwd)/$(BUILD)/testdir

install:
	ninja -C _build install # This will actually install in _build/testdir

run:
	ninja -C _build run

